(require 'ox-publish)

(setq project-ref "notater"
      project-ref-org "org-notater")

(setq org-publish-project-alist
      (list (list project-ref-org
		  :base-directory "./org/"
		  :base-extension "org"
		  :publishing-directory "./public/"
		  :recursive t
		  :publishing-function 'org-html-publish-to-html)
	    (list project-ref
		  :components (list project-ref-org))))

(org-publish-project project-ref)
